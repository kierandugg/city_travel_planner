class ScoreCalculator:

    def __init__(self, crime_rate, public_transport, current_temperature):
        self.crime_rate = crime_rate
        self.public_transport = public_transport
        self.current_temperature = current_temperature

    @property
    def score(self):
        return self.calculate_city_score()

    def calculate_city_score(self):
        """
        Returns a score based on the current temperature,
        the public transport rating, and the crime rate.
        """
        city_score = 0.0
        if 30.0 >= self.current_temperature >= 20.0:
            city_score = 3.3
        city_score += (float(self.public_transport) / 3)
        city_score += (10 - float(self.crime_rate)) / 3
        return city_score
