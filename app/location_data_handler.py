from tornado.web import RequestHandler
from app.city_orchestrator import CityOrchestrator
import json
from http import HTTPStatus


class LocationHandler(RequestHandler):

    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)

    def initialize(self, data):
        self.data=data

    def get(self, city_name):
        """
        Retrieve information about a city.
        """
        city = CityOrchestrator(city_name, self.data).create_city()
        city_info = {'city_name': city.city_name,
                     'current_temperature': city.current_temperature,
                     'current_weather_description': city.current_weather_description,
                     'population': city.population,
                     'bars': city.bars,
                     'city_score': round(city.score, 2)}
        self.set_status(HTTPStatus.OK)
        self.write(json.dumps(city_info))
