from tornado.httpclient import HTTPClient
from tornado.web import HTTPError
from http import HTTPStatus
import json


class WeatherService:

    def __init__(self, city_name):
        self.city_name = city_name
        self.city_id = self._get_city_id()
        self.current_weather = self._get_current_weather()

    @property
    def current_temperature(self):
        return round(self.current_weather['the_temp'], 2)

    @property
    def current_weather_description(self):
        return self.current_weather['weather_state_name']

    def _get_city_id(self):
        """
        Return the ID of the a city.
        """
        query_url = "/location/search/?query=%s" % self.city_name
        response = self._get_weather_data_from_url(query_url)
        if response:
            return response[0]["woeid"]
        else:
            raise HTTPError(HTTPStatus.BAD_REQUEST, "Incorrect response for an unknown city")

    def _get_current_weather(self):
        """
        Return the current weather details of a city.
        """
        query_url = "/location/%s" % self.city_id
        response = self._get_weather_data_from_url(query_url)
        return response["consolidated_weather"][0]

    @staticmethod
    def _get_weather_data_from_url(query_url):
        """
        Returns a response from a URL query.
        """
        base_url = 'https://www.metaweather.com/api'
        http_client = HTTPClient()
        try:
            response = http_client.fetch(base_url + query_url)
        except HTTPError as e:
            print("Error: " + str(e))
        except Exception as e:
            print("Error: " + str(e))

        http_client.close()
        return json.loads(response.body.decode())