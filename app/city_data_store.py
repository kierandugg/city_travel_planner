from os import path
from tornado.web import HTTPError
from http import HTTPStatus
import csv


class CityDataStore:

    def __init__(self):
        self.european_city_info = self._read_data_from_file("european_cities.csv")

    @staticmethod
    def _read_data_from_file(file_name):
        """
        Read and store data from a CSV file
        """
        basepath = path.dirname(__file__)
        filepath = path.abspath(path.join(basepath, "../data", file_name))
        with open(filepath) as european_cities:
            reader = csv.DictReader(european_cities)
            city_list = [row for row in reader]
        return city_list

    def get_city_details(self, city_name, categories):
        """
        Return details of specified city categories.
        """
        city_details = {}
        for row in self.european_city_info:
            if row['city'].lower() == city_name.lower():
                for category in categories:
                    city_details[category] = row[category]
                break
        if city_details:
            return city_details
        else:
            raise HTTPError(HTTPStatus.BAD_REQUEST, "Incorrect response for an unknown city")