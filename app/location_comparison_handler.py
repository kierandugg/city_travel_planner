from tornado.web import RequestHandler
from http import HTTPStatus
import json
from app.city_orchestrator import CityOrchestrator


class LocationComparisonHandler(RequestHandler):

    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)

    def initialize(self, data):
        self.data = data

    def get(self, cities):
        """
        Retrieves information about multiple cities,
        rates them and returns a ranking and score for each city.
        """
        city_data = []
        for city_name in cities.split(','):
            city = CityOrchestrator(city_name.strip(), self.data).create_city()
            city_data.append({'city_name': city.city_name,
                              'city_score': round(city.score, 2)})
        response = {'city_data': self._rank_cities(city_data)}

        self.set_status(HTTPStatus.OK)
        self.write(json.dumps(response))

    @staticmethod
    def _rank_cities(cities):
        current_rank = 1
        cities = sorted(cities, key=lambda k: k['city_score'], reverse=True)
        for city in cities:
            city['city_rank'] = current_rank
            current_rank += 1
        return cities




