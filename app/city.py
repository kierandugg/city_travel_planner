class City:

    def __init__(self, city_name, current_temperature, current_weather_description, population, bars, score):
        self.city_name = city_name
        self.current_temperature = current_temperature
        self.current_weather_description = current_weather_description
        self.population = population
        self.bars = bars
        self.score = score
