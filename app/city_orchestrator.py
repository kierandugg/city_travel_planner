from app.city import City
from app.weather_service import WeatherService
from app.score_calculator import ScoreCalculator


class CityOrchestrator:

    def __init__(self, city_name, city_data):
        self.city_name = city_name
        self.city_data = city_data
        self.weather = WeatherService(city_name)

    def create_city(self):
        city_details = self.city_data.get_city_details(self.city_name,
                                                          ['population',
                                                           'crime_rate',
                                                           'bars',
                                                           'public_transport'])
        city_score = ScoreCalculator(city_details['crime_rate'],
                                     city_details['public_transport'],
                                     self.weather.current_temperature).score

        return City(self.city_name,
                    self.weather.current_temperature,
                    self.weather.current_weather_description,
                    city_details['population'],
                    city_details['bars'],
                    city_score)
